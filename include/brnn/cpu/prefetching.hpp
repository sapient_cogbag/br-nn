/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <brnn/predecl.hpp>
#include <experimental/coroutine>
#include <brnn/coro/scheduler.hpp>
#include <brnn/coro/promise.hpp>

// Credit to https://www.youtube.com/watch?v=j9tlJAqMV7U&feature=youtu.be 
#include <xmmintrin.h>

namespace brnn::cpu {
    using namespace tdefs;
    
    /**
     * CPU cache levels to prefetch to.
     */
    enum class CACHE_LEVEL {
        MINIMISE_POLLUTION = _MM_HINT_NTA,  // Prefetch to cache with minimum cache pollution
        L1 = _MM_HINT_T0,  // Prefetch into all levels of cache (L3/L2/Data L1/etc.)
        L2 = _MM_HINT_T1,  // Prefetch to L3 and L2
        L3 = _MM_HINT_T2  // Prefetch into L3
    };
    
    /**
     * Taken in part from the nanocoroutines video.
     * 
     * T is the type of the value to be retrieved with _mm_prefetch
     * 
     * The cache level is the hint to be passed to _mm_prefetch to 
     * determine which caches to prefetch to.
     * 
     * The timing on this prefetcher needs to be coordinated by the coroutine
     * runner system to control the delay for reentering the coroutine
     * after the prefetch to maximise information transfer rate. 
     * 
     * This expects a symmetric scheduler.
     */
    template <typename T, CACHE_LEVEL cache_level = CACHE_LEVEL::MINIMISE_POLLUTION>
    struct prefetcher_awaiter {
        T& val;
        coro::sched::symmetric_scheduler& scheduler;
        
        prefetcher_awaiter(T& v, coro::sched::symmetric_scheduler& scheduler) : val(v), scheduler(scheduler) {}
        
        scoro::suspend_always await_ready() noexcept {
            return {};
        }
        
        auto await_suspend(scoro::coroutine_handle<> curr_coro) {
            __mm_prefetch(std::addressof(val), cache_level);
            auto next_coro = scheduler.pop_next_scheduled();
            scheduler.schedule_coro(curr_coro);
            return next_coro;
        }
        
        template <typename Promise>
        auto await_suspend(scoro::coroutine_handle<Promise> curr_coro) {
            __mm_prefetch(std::addressof(val), cache_level);
            auto next_coro = scheduler.pop_next_scheduled();
            if (curr_coro.promise().is_active_scheduled) {
                scheduler.schedule_active_coro_unsafe(curr_coro);
            } else {
                scheduler.schedule_coro_unsafe(curr_coro);
            }
            return next_coro;
        }
        
        T& await_resume() {
            return val;
        }
        
    };
    
    
    
    
    
    
    
    
    
}
