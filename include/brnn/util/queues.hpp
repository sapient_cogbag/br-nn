/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <brnn/predecl.hpp>
#include <array>
#include <vector>

namespace brnn::util {
    using namespace tdefs;
    
    /**
     * A queue holding a fixed number of max elements.
     */
    template <typename T, usize _N>
    struct fixed_size_queue {
        // We basically treat this as an array of length _N, but leave a space 
        // at [end] so we can distinguish [end] from [oldest_idx] in the case that 
        // there are _N elements so [oldest_idx + _N] =/= [end] (mod N)
        std::array<T, _N> vals;
        
        // These are both modulo-n and represent what region of the queue is present.
        usize front_idx = 0;
        // This is one-after the end. Since this being equal to oldest_idx could mean we are empty or full,
        // we will indicate as such with the is_full variable.
        usize end_idx = 0;
        
        bool is_full = false;
        
        /**
         * Get a reference to the oldest value in an unsafe manner without popping it off.
         * 
         * Good for prefetching.
         */
        T& get_front_unsafe() {
            return vals[front_idx % _N];
        }
        
        const T& get_front_unsafe() const {
            return vals[front_idx % _N];
        }

        /**
         * Get the oldest value from the queue in an unsafe manner (without checking if it exists).
         */
        T pop_front_unsafe() {
            auto val = vals[front_idx % _N];
            front_idx = (front_idx + 1) % _N;
            is_full = false; // If we were full, make it so we aren't
            return val;
        }
        
        /**
         * Pop from the front, unless empty, then give a default value.
         */
        T pop_front(T default_v) {
            return size() != 0 ? pop_front_unsafe() : default_v;
        }
        
        /**
         * Push back a new value onto the queue in an unsafe manner - i.e. if there is already something
         * there, overwrite it.
         */
        void push_back_unsafe(const T& data) {
            if (size() == _N) { // If we have no more room, wrap around.
                front_idx = (front_idx + 1) % _N;
            }
            vals[end_idx % _N] = data;
            end_idx = (end_idx + 1) % _N;
            is_full = (front_idx == end_idx);
        }
        
        constexpr usize max_size() const {
            return _N;
        }
        
        constexpr usize size() const {
            if (is_full) return _N;
            return (end_idx - front_idx) % _N;
        }
        
        /**
         * Push back a new value onto the queue in a safe manner - specifically, provide 
         * a callback to remove elements from the queue, which will be run until there is space.
         * 
         * The callable should take the queue and clear it.
         */
        template <typename Function>
        void push_back(const T& data, Function&& invokeable) {
            while (is_full) {
                std::invoke(std::forward<Function>(invokeable), *this);
            }
            push_back_unsafe(data);
        }
        
        /**
         * Empty out the queue safely.
         */
        void clear_queue() {
            while(size() != 0) {
                pop_front_unsafe();
            }
        }
    };
    
    
    /**
     * Holds a dynamic sized queue where insertion can be at arbitrary points in the queue IF it is already full.
     * 
     * Note that for efficiency reasons, this does not hold the queue in order 100% of the time. Specifically, 
     * if the queue has to be expanded dynamically, queue elements may end up placed in the middle of the queue
     * until it clears out, essentially delaying things scheduled just before the queue expansion until all new
     * expanded queue things are cleared out.
     */
    template <typename T>
    class dynamic_size_queue {
        std::vector <T> data; // Size is just length of this vector.
        
        bool is_full;
        
        // These are both modulo-length and represent what region of the queue is present.
        usize front_idx = 0;
        // This is one-after the end. Since this being equal to oldest_idx could mean we are empty or full,
        // we will indicate as such with the is_full variable.
        usize end_idx = 0;
        
    public:
        /**
         * We actually start out being full since the initial vector is zero-length.
         */
        dynamic_size_queue() : data(), is_full(true), front_idx(0), end_idx(0) {}
        dynamic_size_queue(usize initial_max_size) : 
            data(initial_max_size), 
            is_full(initial_max_size == 0),
            front_idx(0), 
            end_idx(0) 
            {}
        
        /**
         * Get a reference to the oldest value in an unsafe manner without popping it off.
         * 
         * Note that this does not necessarily return the oldest if there has been a recent expansion.
         * 
         * Good for prefetching.
         */
        T& get_front_unsafe() {
            return data[front_idx % data.size()];
        }
        
        const T& get_front_unsafe() const {
            return data[front_idx % data.size()];
        }
        
        /**
         * Get the front value from the queue in an unsafe manner (without checking if it exists).
         * 
         * The name of this function is a misnomer, it does not necessarily produce the oldest (though it
         * usually will), if there's been a recent queue expansion.
         */
        T pop_front_unsafe() {
            auto val = data[front_idx % data.size()];
            front_idx = (front_idx + 1) % data.size();
            is_full = false; // If we were full, make it so we aren't
            return val;
        }
        
        /**
         * Pop off the front of the queue. If the queue is empty, then we return the default parameter.
         */
        T pop_front(T default_v) {
            return size() != 0 ? pop_front_unsafe() : default_v;
        }
        
        /**
         * Holds the current maximum size of the queue (the size at which any new elements 
         * cause expansion).
         */
        constexpr usize max_size() const {
            return data.size();
        }
        
        constexpr usize size() const {
            if (is_full) return data.size();
            return (end_idx - front_idx) % data.size();
        }
        
        /**
         * This push_back_unsafe is not actually unsafe :)
         */
        void push_back_unsafe(const T& v) {
            if (size() == max_size()) { // If we have no more room, expand.
                data.push_back(v);
                if (end_idx == max_size() - 2) { // old max_size -1 => current max_size - 2
                    end_idx = max_size() - 1; // make current max_size
                }
            } else {
                data[end_idx % max_size()] = v;
                end_idx = (end_idx + 1) % max_size();
                is_full = (front_idx == end_idx);
            }
        }
        
        void push_back(const T& v) {
            push_back_unsafe(v);
        }
        
        constexpr bool empty() const noexcept {
            return size() == 0;
        }
    };
    
    /**
     * Like a fixed-sized queue but the fixed size is determined at runtime.
     */
    template <typename T>
    class dynamic_fixed_size_queue {
        const usize N;
        
        std::vector <T> data;
        
        // These are both modulo-n and represent what region of the queue is present.
        usize front_idx = 0;
        // This is one-after the end. Since this being equal to oldest_idx could mean we are empty or full,
        // we will indicate as such with the is_full variable.
        usize end_idx = 0;
        // We hold this to avoid ambiguity when front_idx == end_idx;
        bool is_full = false;
        
    public:
        /**
         * Construct a fixed-size queue of the given size.
         */
        dynamic_fixed_size_queue(usize size) : N(size), data(size), is_full(size == 0) {}
        
        /**
         * Get a reference to the oldest value in an unsafe manner without popping it off.
         * 
         * Good for prefetching.
         */
        T& get_front_unsafe() {
            return data[front_idx % N];
        }
        
        const T& get_front_unsafe() const {
            return data[front_idx % N];
        }
        
        /**
         * Get the oldest value from the queue in an unsafe manner (without checking if it exists).
         */
        T pop_front_unsafe() {
            auto val = data[front_idx % N];
            front_idx = (front_idx + 1) % N;
            is_full = false; // If we were full, make it so we aren't
            return val;
        }
        
        /**
         * Pop off the front of the queue. If the queue is empty, then we return the default parameter.
         */
        T pop_front(T default_v) {
            return size() != 0 ? pop_front_unsafe() : default_v;
        }
        
        /**
         * Push back a new value onto the queue in an unsafe manner - i.e. if there is already something
         * there, overwrite it.
         */
        void push_back_unsafe(const T& data) {
            if (size() == N) { // If we have no more room, wrap around.
                front_idx = (front_idx + 1) % N;
            }
            this->data[end_idx] = data;
            end_idx = (end_idx + 1) % N;
            is_full = (front_idx == end_idx);
        }
        
        constexpr usize max_size() const {
            return N;
        }
        
        constexpr usize size() const {
            if (is_full) return N;
            return (end_idx - front_idx) % N;
        }
        
        /**
         * Push back a new value onto the queue in a safe manner - specifically, provide 
         * a callback to remove elements from the queue until there is space.
         * 
         * The callable should take the queue and clear it, and it will be run until there is space.
         */
        template <typename Function>
        void push_back(const T& data, Function&& invokeable) {
            while (is_full) {
                std::invoke(std::forward<Function>(invokeable), *this);
            }
            push_back_unsafe(data);
        }
        
        constexpr bool empty() const {
            return size() == 0;
        }
        
    };
    
    /**
     * Holds traits of queues.
     * 
     * Also is a false/true type depending on whether the type *is* a library-made queue.
     * 
     * It also defines a real, more standardised interface for accessing and controlling queues,
     * including std::max_value for max_size for un-fixed-size queues (since there is nothing other
     * than hardware limits stopping continuous element addition).
     */
    template <typename Queue>
    struct queue_traits : std::false_type {
        static constexpr bool is_queue = false;
    };
    
    template <typename Queue>
    struct queue_traits <Queue&> : queue_traits<Queue> {};
    
    template <typename Queue>
    struct queue_traits <const Queue> : queue_traits<Queue> {};
    
    template <typename Queue>
    struct queue_traits <volatile Queue> : queue_traits<Queue> {};
    
    template <typename Queue>
    struct queue_traits <Queue&&> : queue_traits<Queue> {};
    
    
    template <typename T, usize N>
    struct queue_traits <fixed_size_queue<T, N>> : std::true_type {
        static constexpr bool is_fixed_size_v = true;
        using element_type = T;
        
        static constexpr auto max_size = [](const fixed_size_queue<T, N>& v) -> usize {
            return N;
        };
        
        static constexpr auto size = [](const fixed_size_queue<T, N>& v) -> usize {
            return v.size();
        };
        
        static constexpr auto is_empty = [](const fixed_size_queue<T, N>& v) -> usize {
            return v.empty();
        };
    };
    
    template <typename T>
    struct queue_traits <dynamic_fixed_size_queue<T>> : std::true_type {
        static constexpr bool is_fixed_size_v = true;
        using element_type = T;
        
        static constexpr auto max_size = [](const dynamic_fixed_size_queue<T>& v) -> usize {
            return v.max_size();
        };
        
        static constexpr auto size = [](const dynamic_fixed_size_queue<T>& v) -> usize {
            return v.size();
        };
        
        static constexpr auto is_empty = [](const dynamic_fixed_size_queue<T>& v) -> usize {
            return v.empty();
        };
    };
    
    template <typename T>
    struct queue_traits <dynamic_size_queue<T>> : std::true_type {
        static constexpr bool is_fixed_size_v = false;
        using element_type = T;
                
        
        static constexpr auto max_size = [](const dynamic_size_queue<T>& v) -> usize {
            return std::numeric_limits<usize>::max();
        };
        
        static constexpr auto size = [](const dynamic_size_queue<T>& v) -> usize {
            return v.size();
        };
        
        static constexpr auto is_empty = [](const dynamic_size_queue<T>& v) -> usize {
            return v.empty();
        };
    };
    
    template <typename Q>
    constexpr bool is_queue_v = queue_traits<Q>::value;
    
    template <typename Q>
    using queue_element_t = typename queue_traits<Q>::element_type;
    
    /**
     * Run through all elements of a queue.
     * 
     * The invokeable must take a queue and an element, and will be run on the elements until the queue
     * is empty.
     */
    template <typename Queue, typename Invokeable>
    requires is_queue_v<Queue>
    void for_each_queue_element(Queue& queue, Invokeable&& iv) {
        while (!queue_traits<Queue>::is_empty(queue)) {
            std::invoke(iv, queue, queue.pop_front_unsafe());
        }
    }
    
    /**
     * Pop items off the queue until the given condition (which is parameterised by the element,)
     * is true, and pop that one off but return it. If you empty the whole queue then return the default_v.
     */
    template <typename Queue, typename T, typename Invokeable>
    requires is_queue_v<Queue> && std::is_same_v<
        std::decay_t<T>, 
        std::decay_t<queue_element_t<Queue>>
    >
    queue_element_t<Queue> clear_until(Queue& queue, Invokeable&& iv, T default_v) {
        // While we have elements in the queue, pull one off and test the predicate.
        while (!queue.empty()) {
            T queue_elem = queue.pop_front_unsafe();
            if (std::invoke(std::forward<Invokeable>(iv), queue_elem)) return queue_elem;
        }
        return default_v;
    }
}
