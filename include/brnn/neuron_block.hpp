/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <brnn/predecl.hpp>
#include <brnn/neuron_address.hpp>
#include <experimental/coroutine>
#include <type_traits>
#include <optional>

namespace brnn::route {
    using namespace brnn::tdefs;
    
    /**
     * Concepts not fully implemented yet, will implement proper concept when available.
     * 
     * A Routeable object is either a neuron or neuron block. Routeable objects allow someone
     * to push in messages from each of the six directions and push them out similarly, in a 3D
     * routing system. This should be asynchronous.
     * 
     * In short, all the routing blocks at the "ends" of the edges (unless the contained type is a neuron, in which case
     * all neurons) should register their edge router as being the containing router.
     */
    template <typename T> concept Routeable = true;
    
    /**
     * Selects routing direction of a neuron address based on how many bits this address router encompasses.
     * 
     * The held bit count is the number of bits in the address which can be arbitrary values and the location
     * inside each component of the address would be within the routing region of this router.
     */
    template <u8 held_bit_count> 
    struct raw_address_router {
        static constexpr i64 bit_mask = 0xFFFFFFFFFFFFFFFF << held_bit_count;
    protected:
        const neuron_address our_masked_coordinates;
        /**
         * Create a router containing the given address, by using the bitmask.
         * 
         * It is recommended to do simple address generation rather than ad-hoc neuron 
         * testing for addresses.
         */
        raw_address_router(neuron_address contained_address) : our_masked_coordinates{
            .x = contained_address.x & bit_mask,
            .y = contained_address.y & bit_mask,
            .z = contained_address.z & bit_mask
        } {}
        
    public:
        constexpr directions_t routing_direction(neuron_address a) {
            const i64 xmasked = a.x & bit_mask;
            const i64 ymasked = a.y & bit_mask;
            const i64 zmasked = a.z & bit_mask;
            
            const auto dirs = get_cardinal_directions(
                our_masked_coordinates, 
                {.x = xmasked, .y = ymasked, .z = zmasked}
            );
            
            return dirs;
        }
    };
    
    /**
     * A router object for routing router objects that are a subrouter of this.
     * 
     * This is effectively a network router for a network simulation. Pretty neat :)
     * 
     * Each router object has a set number of bits of the address that it contains all
     * elements of. In effect this allows us to determine the routing direction of a packet by a simple
     * bitmask and compare for each of the 3D coordinates.
     */
    template <typename RouteableObject>
    requires Routeable<RouteableObject> 
    class router {
        std::optional <refw<router>> right_router;
        std::optional <refw<router>> left_router;
        std::optional <refw<router>> top_router;
        std::optional <refw<router>> bottom_router;
        std::optional <refw<router>> back_router;
        std::optional <refw<router>> front_router;
    };
    
    
    
}
