/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <brnn/predecl.hpp>

namespace brnn::route {
    using namespace tdefs;
        
    /**
     * Neuron locator triplet.
     */
    struct neuron_address {
        i64 x;
        i64 y;
        i64 z;
    };
    
    /**
     * Get the cardinal directions required to be traversed to get from a to b.
     */
    constexpr directions_t get_cardinal_directions(neuron_address a, neuron_address b) {
        // Results for each cardinal direction on each axis.
        const auto xr = 
            (a.x > b.x) ? u64(DIRECTIONS::LEFT) : 
            (a.x < b.x) ? u64(DIRECTIONS::RIGHT) : 
            u64(DIRECTIONS::NONE);
        const auto yr = 
            (a.y > b.y) ? u64(DIRECTIONS::DOWN) : 
            (a.y < b.y) ? u64(DIRECTIONS::UP) : 
            u64(DIRECTIONS::NONE);
        
        const auto zr = 
            (a.z > b.z) ? u64(DIRECTIONS::BEHIND) : 
            (a.z < b.z) ? u64(DIRECTIONS::FORWARD) : 
            u64(DIRECTIONS::NONE);
            
        return directions_t(xr | yr | zr);
    }
    
    
}
