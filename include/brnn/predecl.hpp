/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once

#include <cstdint>
#include <bitset>
#include <experimental/coroutine>

namespace brnn {
    namespace tdefs {
        using u8 = std::uint8_t;
        using u16 = std::uint16_t;
        using u32 = std::uint32_t;
        using u64 = std::uint64_t;
        
        using i8 = std::int8_t;
        using i16 = std::int16_t;
        using i32 = std::int32_t;
        using i64 = std::int64_t;
        
        using usize = std::size_t;
        
        template <typename T> using refw = std::reference_wrapper<T>;
        
        /**
         * Non-owning/observer pointer type.
         * 
         * This can be nullptr, so be careful :)
         */
        template <typename T> using observer_ptr = T*;
        
        /**
         * S(tandard) coro(utines)
         */
        namespace scoro {
            using namespace std::experimental::coroutines_v1;
        };
    }
    
    using namespace tdefs;
    
    namespace coro {
        enum class CORO_START_TYPE {
            EAGER = 0, // suspend_never
            LAZY = 1 // suspend_always
        };
        
        template <CORO_START_TYPE stype>
        using suspend_awaiter_rtype_t = std::conditional_t<
            stype == CORO_START_TYPE::EAGER,
            scoro::suspend_never,
            scoro::suspend_always
        >;
        
        namespace sched {
            class symmetric_scheduler;
            
            /**
             * Specifies symmetric (direct coroutine-to-coroutine context switch)
             * vs asymmetric (complete and leave scheduler to do it) transfer of coroutine state.
             */
            enum class CORO_SWITCH_TYPE {
                ASYMMETRIC,
                SYMMETRIC
            };
            
            // Selects an appropriate return type for suspending or termination.
            // i.e. if it is eager to suspend and asymmetric then this will produce a suspend_never
            // regardless of eager/not eager to suspend with symmetric coroutine switching we produce
            // a coroutine_handle<>
            template <CORO_SWITCH_TYPE transfer, CORO_START_TYPE eagerness = CORO_START_TYPE::EAGER>
            using suspend_awaiter_transfer_rtype_t = std::conditional_t<
                transfer == CORO_SWITCH_TYPE::ASYMMETRIC,
                suspend_awaiter_rtype_t<eagerness>, // asymmetric/fully through scheduler
                scoro::coroutine_handle<> // Symmetric transfer
            >; 
            
            /**
             * Selects whether to return a coroutine handle to a coro to transfer to 
             * or to return a boolean to return to caller/suspend.
             */
            template <CORO_SWITCH_TYPE transfer>
            using suspend_awaiter_cond_trans_rtype_t = std::conditional_t <
                transfer == CORO_SWITCH_TYPE::ASYMMETRIC,
                bool,
                scoro::coroutine_handle<>
            >;
        }
        
        namespace promise {
            struct base_task;
            struct base_generator;
            
            struct base_task_promise;
            struct base_generator_promise;
            
            template <typename, CORO_START_TYPE, sched::CORO_SWITCH_TYPE>
            struct task_promise;
            
            template <typename>
            struct generator_promise;
        }
        
        namespace tasks {
            template <
                typename T = void, 
                CORO_START_TYPE start_type = CORO_START_TYPE::EAGER,
                sched::CORO_SWITCH_TYPE switch_type = sched::CORO_SWITCH_TYPE::SYMMETRIC
            >
            struct task;
        }
    }
    
    using namespace tdefs;
    using directions_t = std::bitset<6>;
    
    /**
     * Simple cardinal directions.
     * 
     * Note that the more positive the z, the more IN FRONT we are, NOT the further back we are.
     */
    enum class DIRECTIONS {
        NONE = 0,
        RIGHT = 1,
        LEFT = 2,
        UP = 4,
        TOP = UP,
        DOWN = 8,
        BOTTOM = DOWN,
        FRONT = 16,
        BACK = 32,
        BEHIND = BACK,
        FORWARD = FRONT
    };
    
    constexpr directions_t to_dirset(DIRECTIONS d) {
        return directions_t(u64(d));
    }

}
