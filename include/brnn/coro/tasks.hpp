/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <brnn/predecl.hpp>
#include <brnn/coro/scheduler.hpp>
#include <brnn/coro/promise.hpp>

/**
 * Simple coroutine task/promise class.
 * 
 */

namespace brnn::coro::tasks {
    
    namespace _detail {
        struct _base_task {
            bool done = false;
            promise::base_task_promise& our_promise;
            
            bool is_done() const noexcept;
            
            bool await_ready() const noexcept;
            
            auto get_current_scheduler() noexcept -> observer_ptr<sched::symmetric_scheduler>;
            
            auto get_current_scheduler() const noexcept -> observer_ptr<const sched::symmetric_scheduler>;
        };
    }
    
    /**
     * Awaitable base class. Other classes cast to and from the actual type of the promise reference.
     * 
     * Can control behaviour on suspend/resume.
     */
    template <sched::CORO_SWITCH_TYPE coro_transfer> 
    struct base_task : _detail::_base_task {
        using _detail::_base_task::our_promise;
        using _detail::_base_task::done;
        
    protected:
        base_task(promise::base_task_promise& promise) : _detail::_base_task{.done=false, .our_promise=promise} {}
    public:
        
        // No scheduler here so can't do transfer
        bool await_suspend(scoro::coroutine_handle<> outer_coroutine) noexcept {
            return !is_done();
        }
        
        template <typename Promise>
        sched::suspend_awaiter_cond_trans_rtype_t<coro_transfer> 
        await_suspend(scoro::coroutine_handle<Promise> outer_coroutine) noexcept {
            if constexpr (coro_transfer == sched::CORO_SWITCH_TYPE::ASYMMETRIC) {
                // Asymmetric transfer, just return no to suspending if we are done.
                return !is_done();
            } else { // symmetric transfer. Here we need to get the scheduler from the coros promise but
                // if it is nullptr then we conditionally return a noop coroutine to emulate asymmetric behaviour.
                // We also unconditionally set the scheduler on our promise to that of the outer coroutine
                our_promise.last_scheduled_on = outer_coroutine.promise().last_scheduled_on;
                if (is_done()) { // if we are done, then resume the current coro
                    return outer_coroutine;
                } else { // Else spit out whatever is in the scheduler.
                    if (our_promise.last_scheduled_on) {
                        auto res = our_promise.last_scheduled_on->pop_next_scheduled();
                        // We know this is safe since we just made space.
                        our_promise.last_scheduled_on->schedule_coro_unsafe(outer_coroutine);
                        return res;
                    } else {
                        return scoro::noop_coroutine();
                    }
                }
            }
        }
        
    };
    
    /**
     * Created following: https://en.cppreference.com/w/cpp/language/coroutines
     */
    template <
        typename T, 
        CORO_START_TYPE start_type,
        sched::CORO_SWITCH_TYPE switch_type
    >
    struct task : public base_task <switch_type> {
        using promise_type = promise::task_promise<T, start_type, switch_type>;
        
        using base_task<switch_type>::done;
        using base_task<switch_type>::our_promise;
        
        T result;
        
        /**
         * Build ourselves from a promise.
         * 
         * Note that we use the restrictions on our promise to make sure we can do a static cast again.
         */
        task(promise_type& our_promise) : base_task <switch_type>(our_promise) {
            update_our_promise();
        }
        
        task(task&& other) : base_task <switch_type>(other.our_promise) {
            update_our_promise();
        }
        
        
        /**
         * Update our promise with the pointers to our own stuff.
         */
        void update_our_promise() const {
            static_cast<promise_type&>(our_promise).result_ptr = &result;
            static_cast<promise_type&>(our_promise).done_ptr = &done;
        }
        
        T await_resume() const noexcept {
            return result;
        }
    };
    
    template <
        CORO_START_TYPE start_type,
        sched::CORO_SWITCH_TYPE switch_type
    > 
    struct task<void, start_type, switch_type> : public base_task <switch_type> {
        using promise_type = promise::task_promise<void, start_type, switch_type>;
                
        using base_task<switch_type>::done;
        using base_task<switch_type>::our_promise;
        
        /**
         * When we get created, we want to make the promise's result come to us rather than anyone else.
         */
        task(promise_type& promise) : base_task <switch_type> (promise) {
            update_our_promise();
        }
        
        task(task&& other) : base_task<switch_type> (other.our_promise) {
            update_our_promise();
        }
        
        /**
         * Update our promise with the pointers to our own stuff.
         */
        void update_our_promise() {
            static_cast<promise_type&>(our_promise).done_ptr = &done;
        }
        
        void await_resume() const noexcept {}
        
    };
}
