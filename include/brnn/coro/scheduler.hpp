/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#pragma once
#include <brnn/predecl.hpp>
#include <experimental/coroutine>
#include <brnn/util/queues.hpp>

#include <xmmintrin.h>


/**
 * This is a scheduler for coroutines. Or several. These are generally single-threaded until we get some
 * triple buffering for network dispatching coroutines.
 */
namespace brnn::coro::sched {
    using namespace tdefs;
    
    /**
     * Simple base class of schedulers.
     * 
     * Can be used for symmetric scheduling, direct transfer of control to another coroutine as opposed
     * to just returning control to the scheduler. There is nothing to stop asymmetric coroutine scheduling
     * though :)
     */
    class symmetric_scheduler {
    public:
        using chandle_t = scoro::coroutine_handle<>;
        
        /**
         * Get the number of coroutines (done or not) left on the scheduler.
         */
        virtual usize get_size() const noexcept = 0;
        
        /**
         * Start to run all the coroutines.
         */
        virtual void run() noexcept = 0;
        
        /**
         * Add a new coroutine to the list of coroutines to run.
         * 
         * This should be a safe operation - it should not remove any 
         * existing coroutines until they are done.
         */
        virtual void schedule_coro(chandle_t handle) noexcept = 0;
        
        /**
         * Add a new coroutine to the list of coroutines to run, indicating that
         * it should be done "ASAP" and put near the front of the queue if possible/in any
         * "hot pool" for coroutines that should be frequently resumed until done.
         * 
         * Implementations should expect this function to be called maximum of once per
         * suspend-evaluating coroutine to reschedule the existing coroutine if needed, as
         * such only the unsafe call is provided as an api (since no extra space should be taken up)
         * 
         * Default implementation just does standard coroutine scheduling.
         */
        virtual void schedule_active_coro_unsafe(chandle_t handle) noexcept {
            schedule_coro_unsafe(handle);
        }
        
        /**
         * Schedule coroutine in an unsafe manner. This function is
         * allowed to delete an older coroutine (preferrably the oldest) 
         * from the scheduler to make room, if required.
         */
        virtual void schedule_coro_unsafe(chandle_t handle) noexcept = 0;
        
        /**
         * Schedule a coroutine in an unsafe manner. 
         */
        
        /**
         * Simple implementations of promise schedule-setting.
         * 
         * Nonvirtual so that we can do templates and it works for all promise types :)
         */
        template <typename Promise>
        void schedule_coro(scoro::coroutine_handle<Promise> hdl) noexcept {
            hdl.promise().last_scheduled_on = this;
            hdl.promise().is_active_scheduled = false;
            this->schedule_coro(chandle_t(hdl));
        }
        
        template <typename Promise>
        void schedule_coro_unsafe(scoro::coroutine_handle<Promise> hdl) noexcept {
            hdl.promise().last_scheduled_on = this;
            hdl.promise().is_active_scheduled = false;
            this->schedule_coro_unsafe(chandle_t(hdl));
        }
        
        template <typename Promise>
        void schedule_active_coro_unsafe(scoro::coroutine_handle<Promise> hdl) noexcept {
            hdl.promise().last_scheduled_on = this;
            hdl.promise().is_active_scheduled = true;
            this->schedule_active_coro_unsafe(chandle_t(hdl));
        }
        
        /**
         * Get the next scheduled coroutine from the scheduler (and pop it off).
         * 
         * This can be an unsafe operation and return a coroutine in an undefined state or with corrupted
         * memory. Better to use a safe operation.
         */
        virtual chandle_t pop_next_scheduled_unsafe() noexcept = 0;
        
        /**
         * Get the next scheduled coroutine from the scheduler and pop it off, where said coroutine should
         * not be done.
         * 
         * If there are none or all are done, then this should return a coroutine with a boolean conversion
         * to False. The way to do this is to return coroutine_handle<>{};
         */
        virtual chandle_t pop_next_scheduled() noexcept = 0;
        
        virtual ~symmetric_scheduler() noexcept {}
    };
    
    
    /**
     * This queue is a very simple nonexpanding queue which expects coroutines to jump between
     * each other one at a time. It is good for memory access/prefetcher coroutines on one thread.
     * 
     * It is not thread safe, but coroutine handles are nice and simple.
     */
    template <usize QueueSize>
    struct simple_queue_scheduler : public symmetric_scheduler {
        using chandle_t = scoro::coroutine_handle<>;
        using queue_t = util::fixed_size_queue<chandle_t, QueueSize>;
        queue_t coros;
        
        /**
         * Pop the oldest coroutine off the queue unconditionally, even if the memory is broken or the coro
         * is done.
         * This is the coroutine at the front.
         */
        chandle_t pop_off_oldest() {
            return coros.pop_oldest_unsafe();
        }
        
        /**
         * Push back a new coroutine onto the queue of coroutine handles
         * 
         * If there is no space then we go through and pull coroutines
         * off and resume them until more space is left in the queue.
         */
        void push_back(chandle_t h) {
            coros.push_back(h, [] (queue_t& coros) {
                chandle_t next_coro = coros.pop_off_oldest();
                if (next_coro.done()) return;
                next_coro.resume();
                // If that didn't do it, move the coroutine back to the
                // end of the queue
                if(!next_coro.done()) {
                    coros.push_back_force(next_coro);
                }
            });
        }
        
        /**
         * Push back a new coroutine onto the queue of coroutine handles.
         * 
         * If there is no space for more coroutines, we overwrite the currently
         * present coroutine that was added longest ago.
         */
        void push_back_force(chandle_t h) {
            coros.push_back_force(h);
        }
        
        usize number_of_elements() const {
            return coros.size();
        }
        
        virtual usize get_size() const noexcept override final {
            return number_of_elements();
        }
        
        /**
         * Try to pop off the oldest coroutine that has not been processed.
         * 
         * If there are none, then we return a just-created, useless coroutine
         * handle, that should bool-evaluate to false (specifically coroutine_handle<>()).
         */
        chandle_t try_pop_off_oldest() {
            return coros.pop_oldest(chandle_t());
        }
        
        
        /**
         * While we have any coroutines left, keep running them. (in a safe manner, tho risk of stack overflow exists)
         */
        virtual void run() noexcept override final {
            while (auto coro = try_pop_off_oldest()) {
                coro.resume();
                if (!coro.done()) {
                    push_back(coro);
                }
            }
        }
        
        void schedule_coro(chandle_t handle) noexcept override final {
            if (!handle.done()) push_back(handle);
        }
        
        void schedule_coro_unsafe(chandle_t handle) noexcept override final {
            if(!handle.done()) push_back_force(handle);
        }
    };
    
    
    /**
     * A class which allows the user to schedule coroutines such that the number of them 
     * cycled through each time is throttled and new ones are delayed until there is space in this
     * throttled array.
     * 
     * Note that this scheduler expects direct coroutine transfer to occur, and does NOT expect awaiters to 
     * reschedule their suspended coroutines onto this.
     */
    class throttling_scheduler : public symmetric_scheduler {
        const usize throttling_level;
        util::dynamic_fixed_size_queue<chandle_t> curr_coros;
        util::dynamic_size_queue<chandle_t> overall_queue;
        
    public:
        throttling_scheduler(
            usize throttling_level = 8, 
            usize initial_mainqueue_size=1000
        ) : 
        throttling_level(throttling_level), 
        curr_coros(throttling_level), 
        overall_queue(initial_mainqueue_size) 
        {}
        
        /**
         * Refeed the throttling pool from the main queue of coroutines until there is space 
         * for exactly (remaining_throttle_pool_space) coroutines to be directly added. Note that 
         * if there are not enough coros in the main queue to do this then (1) you're gonna run out 
         * of coroutines and (2) it won't fill all the way.
         */
        void refeed_pool(const usize remaining_throttle_pool_space=1) noexcept;
        
        usize get_size() const noexcept override final;
        
        chandle_t pop_next_scheduled() noexcept override final;
    };
}
