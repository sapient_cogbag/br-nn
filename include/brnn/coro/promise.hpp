/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
// Holds some basic promise types.

// The coroutine library here takes some inspiration from 
// https://github.com/lewissbaker/cppcoro
// by lewissbaker licensed under MIT.

// TODO: custom operator new/delete on promise types.

#pragma once
#include <brnn/predecl.hpp>
#include <optional>
#include <brnn/coro/scheduler.hpp>

namespace brnn::coro::promise {
    using namespace tdefs;
    
    struct base_task_promise {
        observer_ptr<sched::symmetric_scheduler> last_scheduled_on;
        bool in_active_scheduled = false; // Optional specifier to allow hot scheduling in schedulers,
        // in collaboration with Awaiters.
        
        void unhandled_exception() {}
    };
    
    struct base_generator_promise {
        observer_ptr<sched::symmetric_scheduler> last_scheduled_on;
    };
    
    
    /**
     * Result pointer here.
     */
    template <
        typename T, 
        CORO_START_TYPE how_to_start = CORO_START_TYPE::EAGER,
        sched::CORO_SWITCH_TYPE how_to_finish = sched::CORO_SWITCH_TYPE::SYMMETRIC
    >
    struct [[nodiscard]] task_promise : base_task_promise {
        using result_awaiter_t = tasks::task <T, how_to_start, how_to_finish>;
        observer_ptr<T> result_ptr = nullptr; // holds the actual result.
        observer_ptr<bool> done_ptr = nullptr; // Holds where to set to true when done.
        
        // Initial suspenders.
        suspend_awaiter_rtype_t<how_to_start> initial_suspend() noexcept {return {};}
        
        result_awaiter_t get_return_object() noexcept {
            return tasks::task<T, how_to_start, how_to_finish>(*this);
        }
        
        // Get the final suspend :)
        // We always want to eagerly finish at the end of the coroutine so leave that as is.
        // This is handled in the task!
        scoro::suspend_never final_suspend() noexcept {
            return {};
        }
        
        template <typename U>
        void return_value(U&& val) noexcept {
            if(result_ptr) *result_ptr = T(std::forward<U>(val));
            if(done_ptr) *done_ptr = true;
        }
    };
    
    /**
     * Void specialisation
     */
    template <
        CORO_START_TYPE hts,
        sched::CORO_SWITCH_TYPE htf
    >
    struct [[nodiscard]] task_promise <void, hts, htf> : base_task_promise {
        using result_awaiter_t = tasks::task <void, hts, htf>;
        observer_ptr<bool> done_ptr = nullptr;
        
        // Initial suspenders
        suspend_awaiter_rtype_t<hts> initial_suspend() noexcept {return {};}
        
        result_awaiter_t get_return_object() noexcept {
            return tasks::task<void, hts, htf>(*this);
        }
        
        // Get the final suspend :)
        // We always want to eagerly finish at the end of the coroutine so leave that as is.
        // This thing is handled in the task not the promise.
        scoro::suspend_never final_suspend() noexcept {
            return {};
        }
        
        void return_void() noexcept {
            if(done_ptr) *done_ptr = true;
        }
    };
    
    
    template <typename T>
    struct [[nodiscard]] generator_promise : base_generator_promise {
        
    };
    
}

