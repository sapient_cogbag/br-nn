# br-nn

Routed Neural Network architecture. Kind of like CRNN but more decentralising and localising of neuron processes.

This program/library is distributed under the AGPLv3

Some of the coroutine api stuff was derived using https://github.com/lewissbaker/cppcoro to help understand the
APIs. That project is licensed under MIT.
