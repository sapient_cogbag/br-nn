/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <brnn/util/queues.hpp>
#include <CLI/CLI.hpp>
#include <string>
#include <vector>
#include <random>
using namespace brnn::tdefs;

enum access {
    SEQ_READ = 1,
    SEQ_WRITE = 2,
    RAN_READ = 4,
    RAN_WRITE = 8
};


int main(int argc, char** argv) {
    auto app = CLI::App("N", "a");
    
    usize memory_coro_taskthrottler_lim = 16;
    usize mem_multiplier = 1024 * 1024;  // 1 mibibyte
    usize mem_val = 40;
    
    
    app.add_option<usize, int>(
        "-t,--throttler-limit", 
        memory_coro_taskthrottler_lim, 
        "Set the number of concurrent memory-access coroutine/awaiters."
    );
    
    try {
        app.parse(argc, argv);
    } catch(const CLI::ParseError &e) {
        return app.exit(e);
    }
    
    
    
}
