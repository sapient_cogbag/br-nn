/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <brnn/util/queues.hpp>
#include <catch2/catch.hpp>
#include <iostream>

using namespace brnn::tdefs;

template <typename T, usize Q>
void print_queue_data (const brnn::util::fixed_size_queue<T, Q>& q) {
    std::cout << Catch::Detail::rangeToString(
        q.vals.begin(),
        q.vals.end()
    ) << "\n";
    std::cout << "oldest_idx:" << q.oldest_idx << " end_idx:" << q.end << '\n';
}


TEST_CASE("Fixed Size Queue Testing", "[brnn][util][queue][fixed_size_queue]") {

    constexpr usize qsize = 200;
    auto q = brnn::util::fixed_size_queue<u64, qsize>();
    
    REQUIRE(q.max_size() == qsize);
    
    constexpr u64 v = 320959025;
    
    SECTION("Insert-Remove Test") {
        //q.clear_queue();

        q.push_back_unsafe(v);
        
        //print_queue_data(q);
        
        //REQUIRE(q.vals == std::array<u64, qsize>{v, 0, 0, 0, 0, 0, 0, 0, 0, 0});
        
        REQUIRE(q.get_front_unsafe() == v);
        REQUIRE(q.pop_front_unsafe() == v);
    }
    
    SECTION("State Test") {
        //q.clear_queue();
        //for (usize i = 0; i < qsize; i++) {
          //  q.push_back_unsafe(v);
            //REQUIRE(q.vals[i % qsize] == v);
        //}
    }
    
    
    
    SECTION("Queue Size Test") {
        //q.clear_queue();
        //print_queue_data(q);
        for (auto i = 0; i < qsize * 100; i++) {
            q.push_back_unsafe(i);
            if (i < q.max_size()) {
                REQUIRE(q.size() == i + 1);
            } else {
                REQUIRE(q.size() == q.max_size());
            }
        }
    }
    q.clear_queue();
    
    for(auto idx = 0; idx < qsize; idx++) {
        q.push_back_unsafe(idx);
    }
    
    REQUIRE(q.size() == q.max_size());
    
    // pop and push back a bunch of times to check every gap combo.
    for (auto gshift = 0; gshift < qsize; gshift++) {
        for (auto i = 0; i < qsize - 1; i++) {
            q.pop_front_unsafe();
            q.push_back_unsafe(i);
        }
        //print_queue_data(q);
        REQUIRE(q.size() == q.max_size());
    }

}

TEST_CASE("Dynamic Sized Queue Testing", "[brnn][util][queue][dynamic_size_queue]") {
    
}
