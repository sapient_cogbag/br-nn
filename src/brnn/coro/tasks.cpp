/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <brnn/coro/tasks.hpp>

namespace brnn::coro::tasks {
    bool _detail::_base_task::is_done() const noexcept {
        return done;
    }
    
    bool _detail::_base_task::await_ready() const noexcept {
        return is_done();
    }
    
    auto _detail::_base_task::get_current_scheduler() noexcept -> observer_ptr<sched::symmetric_scheduler> {
        return our_promise.last_scheduled_on;
    }
            
    auto _detail::_base_task::get_current_scheduler() const noexcept -> observer_ptr<const sched::symmetric_scheduler> {
        return our_promise.last_scheduled_on;
    }
}

