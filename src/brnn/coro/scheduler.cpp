/**
 * BRNN - Recursive Neural Network via Routing in Blocks.
 * Copyright (C) 2020  sapient_cogbag <sapient_cogbag@protonmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <brnn/coro/scheduler.hpp>
#include <brnn/coro/promise.hpp>
#include <brnn/util/util.hpp>

namespace brnn::coro::sched {
    
    usize throttling_scheduler::get_size() const noexcept {
        return curr_coros.size() + overall_queue.size();
    }
    
    
    // TODO: Get a generic, more efficient feeding function to/from queues that doesn't rely on tons of
    // if statements in the push_back functions (i.e. precalculates indices).
    void throttling_scheduler::refeed_pool(const usize remaining_throttle_pool_space) noexcept {
        const usize to_transfer = curr_coros.max_size() - curr_coros.size() - remaining_throttle_pool_space;
        const usize from_transfer = overall_queue.size();
        const usize available_transfer = std::min(to_transfer, from_transfer);
        util::for_n(0, available_transfer, [this](usize){
            curr_coros.push_back_unsafe(overall_queue.pop_front_unsafe());
        });
    }
    
    
    throttling_scheduler::chandle_t throttling_scheduler::pop_next_scheduled() noexcept {
        auto result = chandle_t();
        while (get_size() > 0 and not result) {
            refeed_pool(); // Make sure to refeed the pool from the main queue.
            // Use false-evaluating coroutine_handle<>() as indicator of failure to find one in the queue.
            result = util::clear_until(curr_coros, [](chandle_t coro) -> bool {
                return coro.done();
            }, chandle_t());
        }
        return result;
    }
}
